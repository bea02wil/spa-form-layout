import Form from "./Form";

const FormLayout = () => {
  return (
    <div className="form-layout">
      <div className="form-layout__content">
        <Form />
      </div>
    </div>
  );
};

export default FormLayout;
