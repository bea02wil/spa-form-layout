import { useState, useRef } from "react";

const Status = ({ inputValue, name, onChange }) => {
  const [readOnly, setReadOnly] = useState(true);

  const inputElement = useRef(null);

  const statusChangeHandleClick = () => {
    setReadOnly(false);

    inputElement.current.focus();
  };

  const handleBlur = () => {
    if (!readOnly) {
      setReadOnly(true);
    }
  };

  return (
    <div className="form-layout__status-wrapper">
      <div className="form-layout__container1">
        <h1 className="form-layout__greeting">
          Здравствуйте,{" "}
          <span className="form-layout__greeting-highlight">
            Человек №3596941
          </span>
        </h1>
        <div className="form-layout__status-change">
          <span
            className="form-layout__status-change-content"
            onClick={statusChangeHandleClick}
          >
            Сменить статус
          </span>
        </div>
      </div>
      <div className="form-layout__container2">
        <div className="form-layout__status-tooltip">
          <div className="form-layout__rectangle">
            <input
              type="text"
              value={inputValue}
              className="form-layout__rectangle-content"
              name={name}
              readOnly={readOnly}
              ref={inputElement}
              onBlur={handleBlur}
              onChange={onChange}
            />
            <div className="form-layout__rectangle2"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Status;
