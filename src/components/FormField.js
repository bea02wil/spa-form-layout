import { useState, useEffect } from "react";
import citiesJson from "../data/cities.json";
import Status from "./Status";

const FormField = ({
  fieldType,
  inputType,
  labelName = "",
  labelFor,
  fieldPromt = "",
  field,
  handleChange,
  handleSubmit,
}) => {
  const [cities, setCities] = useState([]);

  useEffect(() => {
    if (fieldType === "select") {
      const sortDescByPopulation = (citiesJson) => {
        citiesJson.sort((a, b) => b.population - a.population);
      };

      const sortByCityName = (a, b) => {
        if (a.city > b.city) {
          return 1;
        }
        if (a.city < b.city) {
          return -1;
        }
        return 0;
      };

      const partSort = (arr, indexFrom, indexTo, sortFunc) => {
        const exactIndexFrom = Math.min(indexFrom, indexTo);
        const exactIndexTo = Math.max(indexFrom, indexTo);

        const tempArr = new Array(exactIndexTo - exactIndexFrom + 1);
        tempArr.fill(0);
        let j = 0;
        for (let i = exactIndexFrom; i <= exactIndexTo; i++) {
          tempArr[j] = arr[i];
          j++;
        }

        tempArr.sort(sortFunc);

        j = 0;
        for (let i = exactIndexFrom; i <= exactIndexTo; i++) {
          arr[i] = tempArr[j];
          j++;
        }

        return arr;
      };

      sortDescByPopulation(citiesJson);

      const filteredCities = citiesJson.filter((obj) => obj.population > 50000);

      const sortedCitiesByName = partSort(
        filteredCities,
        1,
        filteredCities.length - 1,
        sortByCityName
      );

      setCities(sortedCitiesByName);
    }
    // eslint-disable-next-line
  }, []);

  switch (fieldType) {
    case "status":
      return (
        <Status
          inputValue={fieldPromt}
          name={labelFor}
          onChange={handleChange}
        />
      );

    case "select":
      return (
        <div className="form-layout__select-wrapper">
          <label htmlFor={labelFor} className="form-layout__label">
            {labelName}
          </label>
          <select
            value={field}
            id={labelFor}
            name={labelFor}
            onChange={handleChange}
            className="form-layout__select"
          >
            {cities.map((obj) => (
              <option key={obj.city}>{obj.city}</option>
            ))}
          </select>
        </div>
      );

    case "button":
      return (
        <div className="form-layout__button-wrapper">
          <button
            type="submit"
            className="form-layout__button-submit"
            onClick={handleSubmit}
          >
            Изменить
          </button>
          <p className="form-layout__field-promt">{fieldPromt}</p>
        </div>
      );

    default:
      return (
        <div id={labelFor} className="form-layout__input-wrapper">
          <div className="form-layout__input">
            <label htmlFor={labelFor} className="form-layout__label">
              {labelName}
            </label>
            <div className="form-layout__input-block">
              <input
                type={inputType}
                className={
                  field.error
                    ? ["form-layout__input-box", "form-layout__error"].join(" ")
                    : "form-layout__input-box"
                }
                id={labelFor}
                name={labelFor}
                value={field.value !== undefined ? field.value : field}
                checked={field}
                onChange={handleChange}
              />
              {field.error ? (
                <p className="form-layout__error-message">{field.error}</p>
              ) : null}
            </div>
          </div>
          <p className="form-layout__field-promt">{fieldPromt}</p>
        </div>
      );
  }
};

export default FormField;
