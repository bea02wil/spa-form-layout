import { useState } from "react";
import FormField from "./FormField";

const Form = () => {
  const initalState = {
    status: "Прежде чем действовать, надо понять",
    city: "",
    password: {
      value: "",
      error: "",
    },
    passwordAgain: {
      value: "",
      error: "",
    },
    email: {
      value: "",
      error: "",
    },
    isAgree: true,
  };
  const [field, setField] = useState(initalState);

  const [lastChanges, setLastChanges] = useState(
    "последние изменения 15 мая 2012 в 14:55:17"
  );

  const isFormValid = () => {
    let passwordError = "";
    let passwordAgainError = "";
    let emailError = "";

    const passwordValidation = () => {
      if (!field.password.value) {
        passwordError = "Укажите пароль";
      } else if (field.password.value.length < 5) {
        passwordError = "Используйте не менее 5 символов";
      }

      return passwordError ? false : true;
    };

    const passwordAgainValidation = () => {
      if (!field.passwordAgain.value) {
        passwordAgainError = "Укажите пароль";
      } else if (field.passwordAgain.value !== field.password.value) {
        passwordAgainError = "Пароли не совпадают";
      }

      return passwordAgainError ? false : true;
    };

    const emailVaildation = () => {
      const emailRegex =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (!field.email.value) {
        emailError = "Укажите E-mail";
      } else if (!emailRegex.test(String(field.email.value).toLowerCase())) {
        emailError = "Неверный E-mail";
      }

      return emailError ? false : true;
    };

    const isPasswordValid = passwordValidation();
    const isPasswordAgainValid = passwordAgainValidation();
    const isEmailVaild = emailVaildation();

    if (isPasswordValid && isPasswordAgainValid && isEmailVaild) {
      console.log("form valid");

      const getCurrentDate = () => {
        const currentDate = new Date();
        const months = [
          "января",
          "февраля",
          "марта",
          "апреля",
          "мая",
          "июня",
          "июля",
          "августа",
          "сентября",
          "октября",
          "ноября",
          "декабря",
        ];

        return {
          day: String(currentDate.getDate()).padStart(2, "0"),
          month: months.find(
            (month, index) => index === +String(currentDate.getMonth() + 1) - 1
          ),
          year: currentDate.getFullYear(),
          hours: String(currentDate.getHours()).padStart(2, "0"),
          minutes: String(currentDate.getMinutes()).padStart(2, "0"),
          seconds: String(currentDate.getSeconds()).padStart(2, "0"),
        };
      };

      const lastChanges = `последние изменения ${getCurrentDate().day} ${
        getCurrentDate().month
      } ${getCurrentDate().year} в ${getCurrentDate().hours}:${
        getCurrentDate().minutes
      }:${getCurrentDate().seconds}`;

      const json = JSON.stringify(field);

      setLastChanges(lastChanges);
      console.log(json);
      setField(initalState);
    } else {
      setField({
        ...field,
        password: { value: field.password.value, error: passwordError },
        passwordAgain: {
          value: field.passwordAgain.value,
          error: passwordAgainError,
        },
        email: { value: field.email.value, error: emailError },
      });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    isFormValid();
  };

  const handleChange = (e) => {
    const isPasswordOrEmail = (name) => {
      if (name === "password" || name === "passwordAgain" || name === "email") {
        return true;
      }

      return false;
    };

    const name = e.target.name;
    let value =
      e.target.type !== "checkbox" ? e.target.value : e.target.checked;
    value = isPasswordOrEmail(name) ? { value, error: "" } : value;

    setField({ ...field, [name]: value });
  };

  return (
    <form className="form-layout__form" onSubmit={handleSubmit}>
      <FormField
        fieldType="status"
        fieldPromt={field.status}
        labelFor="status"
        field={field}
        setField={setField}
        handleChange={handleChange}
      />

      <FormField
        fieldType="select"
        labelName="Ваш город"
        labelFor="city"
        field={field.city}
        handleChange={handleChange}
      />

      <hr className="form-layout__line" />

      <FormField
        inputType="password"
        labelName="Пароль"
        labelFor="password"
        fieldPromt="Ваш новый пароль должен содержать не менее 5 символов."
        field={field.password}
        handleChange={handleChange}
      />
      <FormField
        inputType="password"
        labelName="Пароль еще раз"
        labelFor="passwordAgain"
        fieldPromt={
          "Повторите пароль, пожалуйста, это обезопасит вас с нами\nна случай ошибки."
        }
        field={field.passwordAgain}
        handleChange={handleChange}
      />

      <hr className="form-layout__line" />

      <FormField
        inputType="email"
        labelName="Электронная почта"
        labelFor="email"
        fieldPromt="Можно изменить адрес, указанный при регистрации."
        field={field.email}
        handleChange={handleChange}
      />
      <FormField
        inputType="checkbox"
        labelName="Я согласен"
        labelFor="isAgree"
        fieldPromt="принимать актуальную информацию на емейл"
        field={field.isAgree}
        handleChange={handleChange}
      />
      <FormField
        fieldType="button"
        fieldPromt={lastChanges}
        handleSubmit={handleSubmit}
      />
    </form>
  );
};

export default Form;
