import "./App.scss";
import FormLayout from "./components/FormLayout";

const App = () => {
  return <FormLayout />;
};

export default App;
